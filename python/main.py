import json
import socket
import sys
import math

class Calculator(object):

    def __init__(self):
        self.ticks = 0
        self.accMap = {
            'index': 0,
            'previous': {
                'speed': 0.0,
                'acceleration': 0.0,
                'distance': 0.0
            },
            'current': {
                'speed': 0.0,
                'acceleration': 0.0,
                'distance': 0.0
            }
        }
        self.schema = None
        self.preferred_lane = None

    def calc_bend_lenght(self, angle, radius):
        angle = math.fabs(angle)
        return (angle/360)*2*math.pi*radius

    def simple_track_analysis(self, pieces, lanes):
        preferred_lane = None
        left = 0
        right = 0
        first_switch = None
        for idx, piece in enumerate(pieces):
            if first_switch == None and piece.get("switch", None) == True:
                first_switch = idx
            #we are going to calculate only angles
            angle = piece.get("angle", 0)
            if angle > 0:
                right += self.calc_bend_lenght(angle, piece.get("radius", 0))
            elif angle < 0:
                left += self.calc_bend_lenght(angle, piece.get("radius", 0))

        if first_switch != None:
            # there are more turns to left than right but position is not at first track!
            if left > right :
                preferred_lane = (min((val) for (idx, val) in enumerate(lanes)))["index"]
            elif right > left :
                preferred_lane = (max((val) for (idx, val) in enumerate(lanes)))["index"]

        return preferred_lane

    def get_turn_side(self, left, right):
        if right > left:
            return "Right"
        elif left > right:
            return "Left"
        return None

    def lanes_analysis(self, lanes):
        def flatten_lanes(tmp):
            ret = []
            for t in tmp:
                ret.append(t["index"])
            return ret

        analysed_lanes = {
            "Left": flatten_lanes(sorted(lanes)),
            "Right": flatten_lanes(sorted(lanes, reverse=True))
        }

        print("Lanes turn weight after analysis:\n{0}".format(analysed_lanes))
        return analysed_lanes


    def segmented_track_analysis(self, pieces):
        segments = []
        tmp = None
        segment = []
        for idx, piece in enumerate(pieces):
            if piece.get("switch", False) == True:
                if tmp != None:
                    segments.append(segment)
                else:
                    tmp = segment
                segment = []
            piece["index"] = idx
            segment.append(piece)
        #last segment is appended by tmp pieces (initial ones)
        segment.extend(tmp)
        segments.append(segment)

        #we have now successfully segmented track with segments begining 
        print("We have total {0} segments:\n".format(len(segments)))

        analysed_segments = []
        #time to analyse each segment
        for idx, tmp_pices in enumerate(segments):
            left, right = 0.0, 0.0
            continous_angle = False
            for idx, tmp_pic in enumerate(tmp_pices):
                #we are going to calculate only angles #TODO: ignore some angles
                angle = tmp_pic.get("angle", 0)
                if angle == 0:
                    continous_angle = False
                elif (angle != 0 and continous_angle == True) or (angle > 22.5 or angle < -22.5):
                    if angle > 0:
                        right += self.calc_bend_lenght(angle, tmp_pic.get("radius", 0))
                        continous_angle = True
                    elif angle < 0:
                        left += self.calc_bend_lenght(angle, tmp_pic.get("radius", 0))


            tmp_segment_data = {
                "start_index": tmp_pices[0]["index"],
                "end_index": tmp_pices[len(tmp_pices)-1]["index"],
                "pieces": tmp_pices,
                "left_turns": left,
                "right_turns": right,
                "turns": self.get_turn_side(left, right)
            }
            analysed_segments.append(tmp_segment_data)
            print("{0}\n".format(tmp_segment_data))

        return analysed_segments

    def get_switch_schema(self, current_index, current_lane_index):
        analysed_lanes, analysed_segments = self.analysed_lanes, self.analysed_segments
        schema = {}
        turns = None
        idx = None
        tmp_lane_index = current_lane_index

        #TODO: should first find where are we based on piece current_index... but who cares?
        for segment in analysed_segments:
            turns = segment.get("turns", None)
            if turns != None: #ok so this one turns so maybe change lane?
                idx = analysed_lanes[turns].index(tmp_lane_index)
                if idx > 0: #ok so we should switch
                    schema[segment["start_index"]] = turns
                    tmp_lane_index = analysed_lanes[turns][idx-1]

        print("Switch lanes schema based on current lane: {0}\n{1}".format(current_lane_index, schema))
        return schema

    def parse_track(self, data):
        print("Parsing track data")
        self.track = data
        self.analysed_segments = self.segmented_track_analysis(data["pieces"])
        self.analysed_lanes = self.lanes_analysis(data["lanes"])

    def get_track(self):
        return self.track

    def set_color(self, color):
        self.color = color

    def set_ticks(self, ticks):
        if ticks != None :
            self.ticks = ticks

    def should_switch(self, data):
        #we are checking if now we should switch
        return self.schema.get(self.accMap['index']+1, None)

    def calc_acceleration(self, deltaSpeed, deltaTime):
        return deltaSpeed / deltaTime

    def calc_switch_schema(self, data):
        my_position = self.get_my_position(data)
        self.schema = self.get_switch_schema(my_position["piecePosition"]["pieceIndex"], my_position["piecePosition"]["lane"]["startLaneIndex"])

    def calc_position_data(self, data):
        my_position = self.get_my_position(data)

        if my_position['piecePosition']['pieceIndex'] != self.accMap['index'] :
            self.accMap['index'] = my_position['piecePosition']['pieceIndex']
            self.accMap['current']['distance'] = my_position['piecePosition']['inPieceDistance']
            return False
        else :
            self.accMap['previous']['speed'] = self.accMap['current']['speed']
            self.accMap['previous']['acceleration'] = self.accMap['current']['acceleration']
            self.accMap['previous']['distance'] = self.accMap['current']['distance']

            dist = my_position['piecePosition']['inPieceDistance'] - self.accMap['previous']['distance']
            self.accMap['current']['distance'] = my_position['piecePosition']['inPieceDistance']
            self.accMap['current']['speed'] = dist/1
            self.accMap['current']['acceleration'] = self.calc_acceleration(self.accMap['current']['speed']-self.accMap['previous']['speed'], 1)
            print("Current speed: {0}".format(self.accMap['current']['speed']))
            print("Current acceleration: {0}".format(self.accMap['current']['acceleration']))

        tmp = self.track["pieces"][self.accMap['index']]
        length = tmp.get("length", None)
        if length == None:
            length = self.calc_bend_lenght(tmp.get("angle", 360), tmp.get("radius", 0))

        if self.accMap['current']['distance'] + (4*self.accMap['current']['speed']) + (4*self.accMap['current']['acceleration']) > length and self.accMap['current']['distance'] + (3*self.accMap['current']['speed']) + (3*self.accMap['current']['acceleration']) < length:
            return True

        return False

    def get_my_position(self, data):
        for car in data:
            if car["id"]["color"] == self.color:
                return car
        return None

    def get_tick_throttle(self, positionData):
        throttle = 0.641
        print("Tick throttle calculation {0}".format(self.ticks))
        return throttle

class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.calc = Calculator()
        self.schema_done = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")
        print("SENDING {0}".format(msg))

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        print("throttle {0}".format(throttle))
        self.msg("throttle", throttle)

    def switch(self, where):
        print("switch {0}".format(where))
        self.msg("switchLane", where)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        print(data)
        self.ping()

    def on_game_start(self, data):
        print("Race started")
        print(data)
        self.ping()

    def on_car_positions(self, data):
        print("on_car_positions")
        print(data)
        
        if self.schema_done != True:
            self.calc.calc_switch_schema(data)
            self.schema_done = True

        where = None

        #self.calc.calc_position_data(data)

        if self.calc.calc_position_data(data) == True: #will switch lanes!
            where = self.calc.should_switch(data)

        print("In my opinion we should switch to: {0}".format(where))

        if where == None:
            self.throttle(self.calc.get_tick_throttle(data))
        else:
            self.switch(where)

        #self.throttle(self.calc.get_tick_throttle(data))

    def on_crash(self, data):
        print("Someone crashed")
        print(data)
        self.ping()

    def on_game_init(self, data):
        print("Race init")
        print(data)
        self.calc.parse_track(data["race"]["track"])
        self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print(data)
        self.ping()

    def on_your_car(self, data):
        print("my Car is: {0}".format(data))
        self.calc.set_color(data["color"])
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameInit': self.on_game_init,
            'gameEnd': self.on_game_end,
            'yourCar': self.on_your_car,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            self.calc.set_ticks(msg.get('gameTick', None))
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                print(data)
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()
